<div class="tab_switcher">
<?php
   $list = array();
   foreach ($content as $c)
     $list[] = l($c->title, '', array('fragment' => $c->id, 'attributes' => array('rel' => $c->id)));
   print theme_item_list($list, NULL, 'ul', array('class' => 'tab_list'));
?>
  <div class="tab_container">
    <?php
       foreach ($content as $c)
         print theme('item_list', $c->content, $c->title, 'ul', array('id' => $c->id, 'class' => 'tab-content'));
     ?>
  </div>
</div>
