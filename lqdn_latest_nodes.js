
(function( $, doc, undefined ) {

$(doc).ready(function() {

  var $tab_switcher = $(".tab_switcher"),
      $tabs = $tab_switcher.find(".tab_list a"),
      $tab_container = $tab_switcher.find(".tab_container");

  $tabs.click(function( e ) {
    e.preventDefault();
    $tabs.removeClass("active");
    $(this).addClass("active");
    $tab_container
      .children("div").hide()
      .end()
      .find( "#" + this.rel ).parent().show();
  });

  $tabs.eq(0).click();

});

})( jQuery, document );
