<span class="actuvignette" style="background-image:url('<?php print htmlspecialchars($node->image); ?>');"></span>
<h3><?php print l(trim($node->title), $node->link, $node->link_options); ?></h3>
<p class="<?php print $node->date_class; ?>"><?php print $node->date; ?></p>
<?php print $node->tags; ?>
<div class="teaser"><?php print $node->teaser; ?></div>
