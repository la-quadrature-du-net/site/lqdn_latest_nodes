<div class="very-last-news">
  <h2 class="title">
    <a href="/fr/fuite-lue-fait-pression-pour-la-criminalisation-des-usages-non-commerciaux-dans-lacta">Fuite : L'UE fait pression pour la criminalisation des usages non-commerciaux dans l'ACTA</a>
  </h2>
  <ul class="metas">
    <li class="submitted"><?=$date?>24 Juin 2010</li>
    <li class="tag"><?=$tags?>ACTA</li>
  </ul>
  <p class="content">
    <?=$content?><br />
    <a class="more" href="<?=$link?>">Lire la suite</a> (<a href="/fr/fuite-lue-fait-pression-pour-la-criminalisation-des-usages-non-commerciaux-dans-lacta#attachments">2 fichiers attachés</a>)</p>
  <div class="share-links"></div>
</div>
